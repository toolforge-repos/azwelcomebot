import json
import pywikibot
from sseclient import SSEClient as EventSource
from pywikibot.page import Page


def process_change(change):
    wiki = 'azwiki'
    if change['wiki'] == wiki and change['type'] == 'log' and change['log_type'] == 'newusers':
        user = change['user']
        page_title = f'İstifadəçi müzakirəsi:{user}'
        new_page = Page(pywikibot.Site(), page_title)
        new_page.text = '{{Xoşgəldiniz}}'
        new_page.save("Vikipediyaya xoş gəldiniz!")


def main():
    url = 'https://stream.wikimedia.org/v2/stream/recentchange'

    for event in EventSource(url):
        if event.event == 'message':
            try:
                change = json.loads(event.data)
            except ValueError:
                continue

            process_change(change)


if __name__ == "__main__":
    main()